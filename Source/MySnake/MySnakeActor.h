// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MySnakeActor.generated.h"

UCLASS()
class MYSNAKE_API AMySnakeActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMySnakeActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	int32 SnakeSize = 15;

	float StepSnake = 45.f;

	TArray<UStaticMeshComponent*> SnakeBody;
	class USphereComponent* MyRootComponent;

	void CreateSnakeBody();

	UPROPERTY(EditAnywhere) int32 VisibleBodyChank = 3;

	void SetVisibleChank();

	UPROPERTY(EditAnywhere) FVector2D DirectionMoveSnake;

	float StepDelay = 0.5f;
	float BuferTime = 0;

	void MoveSnake();
	int32 score = 0;

	class AMyPawnCamera* WhoPawn;
	void HaveDamage();
};
