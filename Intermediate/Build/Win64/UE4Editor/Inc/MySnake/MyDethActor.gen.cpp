// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MySnake/MyDethActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMyDethActor() {}
// Cross Module References
	MYSNAKE_API UClass* Z_Construct_UClass_AMyDethActor_NoRegister();
	MYSNAKE_API UClass* Z_Construct_UClass_AMyDethActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_MySnake();
	ENGINE_API UClass* Z_Construct_UClass_UBoxComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInstance_NoRegister();
// End Cross Module References
	void AMyDethActor::StaticRegisterNativesAMyDethActor()
	{
	}
	UClass* Z_Construct_UClass_AMyDethActor_NoRegister()
	{
		return AMyDethActor::StaticClass();
	}
	struct Z_Construct_UClass_AMyDethActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MyRootComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MyRootComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WallColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WallColor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AMyDethActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_MySnake,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMyDethActor_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MyDethActor.h" },
		{ "ModuleRelativePath", "MyDethActor.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMyDethActor_Statics::NewProp_MyRootComponent_MetaData[] = {
		{ "Category", "MyDethActor" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "MyDethActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMyDethActor_Statics::NewProp_MyRootComponent = { "MyRootComponent", nullptr, (EPropertyFlags)0x0010000000080009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMyDethActor, MyRootComponent), Z_Construct_UClass_UBoxComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AMyDethActor_Statics::NewProp_MyRootComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMyDethActor_Statics::NewProp_MyRootComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMyDethActor_Statics::NewProp_WallColor_MetaData[] = {
		{ "Category", "MyDethActor" },
		{ "ModuleRelativePath", "MyDethActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMyDethActor_Statics::NewProp_WallColor = { "WallColor", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMyDethActor, WallColor), Z_Construct_UClass_UMaterialInstance_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AMyDethActor_Statics::NewProp_WallColor_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMyDethActor_Statics::NewProp_WallColor_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AMyDethActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMyDethActor_Statics::NewProp_MyRootComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMyDethActor_Statics::NewProp_WallColor,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AMyDethActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AMyDethActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AMyDethActor_Statics::ClassParams = {
		&AMyDethActor::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AMyDethActor_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_AMyDethActor_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AMyDethActor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AMyDethActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AMyDethActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AMyDethActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AMyDethActor, 3732365605);
	template<> MYSNAKE_API UClass* StaticClass<AMyDethActor>()
	{
		return AMyDethActor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AMyDethActor(Z_Construct_UClass_AMyDethActor, &AMyDethActor::StaticClass, TEXT("/Script/MySnake"), TEXT("AMyDethActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMyDethActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
